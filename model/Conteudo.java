package model;

import java.util.ArrayList;

public class Conteudo {
	//Atributos
	private String objetivo;
	private String autor;
	private ArrayList<String> comentarios;
	
	//Construtor
	public Conteudo (String objetivo, String autor){
		this.objetivo = objetivo;
		this.autor = autor;
	}
	
	//Metodos
	public String getObjetivo() {
		return objetivo;
	}

	public void setObjetivo(String objetivo) {
		this.objetivo = objetivo;
	}

	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}
	
	public void criarComentario(String umComentario){
		this.comentarios.add(umComentario);
	}
	
	public void removerComentario(String umComentario){
		this.comentarios.remove(umComentario);
	}
	
	public String pesquisarComentario(String umComentario){
		for(String comentarioPesquisado : comentarios){
			if(comentarioPesquisado.equalsIgnoreCase(umComentario))return comentarioPesquisado;
			
		}return null;
	}	

}
