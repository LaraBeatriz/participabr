package model;

public class Manifestacao extends Conteudo{
	//Atributos
	private String data;
	private String local;
	private String hora;
	
	//Contrutor
	public Manifestacao (String objetivo, String autor, String data, String local, String hora){
		super(objetivo, autor);
		this.data = data;
		this.local = local;
		this.hora = hora;
	}
	
	//Metodos
	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getLocal() {
		return local;
	}

	public void setLocal(String local) {
		this.local = local;
	}

	public String getHora() {
		return hora;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

	
}
