package model;

public class Proposta extends Conteudo{
	//Atributos
	private String publicoAlvo;
	private String descricaoProblema;
	private String solucaoProblema;
	private boolean isAprovado;
	
	//Construtor
	public Proposta (String objetivo, String autor,String publicoAlvo, String descricaoProblema, String solucaoProblema){
		super(objetivo, autor);
		this.publicoAlvo = publicoAlvo;
		this.descricaoProblema = descricaoProblema;
		this.solucaoProblema = solucaoProblema;
		boolean isAprovado = false;	
	}
	
	//Metodos
	public String getPublicoAlvo() {
		return publicoAlvo;
	}

	public void setPublicoAlvo(String publicoAlvo) {
		this.publicoAlvo = publicoAlvo;
	}

	public String getDescricaoProblema() {
		return descricaoProblema;
	}

	public void setDescricaoProblema(String descricaoProblema) {
		this.descricaoProblema = descricaoProblema;
	}

	public String getSolucaoProblema() {
		return solucaoProblema;
	}

	public void setSolucaoProblema(String solucaoProblema) {
		this.solucaoProblema = solucaoProblema;
	}

	public boolean isAprovado() {
		return isAprovado;
	}

	public void setAprovado(boolean isAprovado) {
		this.isAprovado = isAprovado;
	}

}
