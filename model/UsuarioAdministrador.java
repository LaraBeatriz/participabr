package model;

import java.util.ArrayList;

public class UsuarioAdministrador extends Usuario{
	
	public UsuarioAdministrador (String nome, String email, String login, String senha, String estado){
		super(nome, email, login, senha,estado);
	}

	public ArrayList<Usuario> removerUsuario(ArrayList<Usuario> listaUsuario, Usuario usuarioRemovido){
		listaUsuario.remove(usuarioRemovido);
		return listaUsuario;
	}
	
	public ArrayList<Conteudo> removerConteudo(ArrayList<Conteudo> listaConteudo, Conteudo conteudoRemovido){
		listaConteudo.remove(conteudoRemovido);
		return listaConteudo;
	}
	
}
