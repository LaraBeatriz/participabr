package model;

public class UsuarioGoverno extends Usuario{
	//Atributos
	private String partido;
	private String cargoPolitico;
	
	//Construtor
	public UsuarioGoverno(String nome, String email, String login, String senha, String estado, String partido, String cargoPolitico){
		super(nome, email, login, senha, estado);
		this.partido = partido;
		this.cargoPolitico = cargoPolitico;
	}
	
	//Metodos
	public String getPartido() {
		return partido;
	}

	public void setPartido(String partido) {
		this.partido = partido;
	}

	public String getCargoPolitico() {
		return cargoPolitico;
	}

	public void setCargoPolitico(String cargoPolitico) {
		this.cargoPolitico = cargoPolitico;
	}
	
	public String aprovarProposta(Proposta solucaoProblema){
		solucaoProblema.setAprovado(true);
		return "Solucao Aprovada";
	}

	

}
